library(readxl)
library(dplyr)

setwd("/Users/josephusset/Desktop/biomarkers/extra_data/validation/vhio/somatic/dna_data/")
files <- list.files()

tmbs <- data.frame()
for( i in files[-1]){
  my_data <- read_excel(i, sheet = 1)
  snps <- my_data$dbSNP
  ll <- length(snps)
  tmp <- data.frame( 
    patient = i, 
    check = snps[ll-1], 
    tmb = snps[ll],
    vus = sum(my_data$Classification == "VUS",na.rm = TRUE),
    tot = sum(!is.na(my_data$Classification),na.rm = TRUE),
    rows = ll
  )    
  tmbs <- rbind( tmbs, tmp)
}

tmbs$LAB.ID <- unlist(lapply( tmbs$patient, function(i) paste(strsplit(i,".xlsx")[[1]][c(1)], collapse = "")))
saveRDS(tmbs, "vhio_tmb.Rds")
