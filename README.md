immune_biomarkers

## Content
* Repo contains code to reproduce analyses for immune biomarkers project (https://bitbucket.org/bbglab/hartwig_biomarkers/src/master/). 

## Prerequisites

#### Source data
* Initial input data is output from the hartwig_biomarkers pipeline.
 
## Running the pipeline
* The pipeline should be run as follows from the root directory:
```
$ python run.py
```

### Issues
* Needs code review and more QA.
* I have not used a conda environment during development. I don't know how this will affect the running.

